set number relativenumber
set colorcolumn=80
set mouse-=a
syntax on

set expandtab
set shiftwidth=4
set tabstop=4
set softtabstop=4
set autoindent
set smartindent
set cindent
set backspace=indent,eol,start

au! BufNewFile,BufReadPost *.{yaml,yml} set filetype=yaml
filetype plugin indent on

set showmatch
set nowrap
set ignorecase

call plug#begin('~/.vim/plugged')

Plug 'davewongillies/vim-eyaml'
Plug 'tpope/vim-fugitive'

Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
autocmd BufNewFile,BufRead *.go setlocal noexpandtab tabstop=2 shiftwidth=2
let g:go_fmt_command = "goimports"
let g:go_term_enabled = 1
let g:go_term_mode = "split"
let g:go_term_height = 13
let g:go_fmt_command = "goimports"


Plug 'NLKNguyen/papercolor-theme'
set t_Co=256
set background=light
let g:PaperColor_Theme_Options = {
    \   'theme': {
    \     'default': {
    \       'allow_bold': 1,
    \       'allow_italic': 1
    \     }
    \   }
    \ }
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
let g:airline_theme='papercolor'

call plug#end()

colorscheme PaperColor 
